package com.shipra.httpServer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author shipra
 *
 */
public class HttpRequestProcessor {
	private HttpRequest request = null;
	private final static String SC_OK = "200 OK";
	private final static String SC_NOT_FOUND = "404 Not Found";
	private final static String SC_FORBIDDEN = "403 Forbidden";
	private final static String SC_BAD_REQUEST = "400 Bad Request";
	private final static String SC_INTERNAL_SERVER_ERROR = "500 Server Error";
	private final static String HTTP_VERSION_1_0 = "HTTP/1.0";
	private final static String HTTP_VERSION_1_1 = "HTTP/1.1";
	private final Map<String, String> contentType = new HashMap<>();

	public HttpRequestProcessor(HttpRequest request) {
		this.request = request;
		contentType.put("html", "text/html");
		contentType.put("shtml", "text/html");
		contentType.put("htm", "text/html");
		contentType.put("txt", "text/html");
		contentType.put("jpeg", "image/jpeg");
		contentType.put("jpg", "image/jpeg");
		contentType.put("css", "text/css");
		contentType.put("gif", "image/gif");
	}

	public HttpResponse requestProcessor() {
		String status = null;
		String fileExtension = "txt";
		byte[] fileBuffer = null;
		//  buffer
		if (request.getHttpRequestType() == null || request.getRequestUri() == null || request.getHttpVersion() == null) {
			// create HttpResponse 400 BAd request
			status = SC_BAD_REQUEST;
			fileBuffer = SC_BAD_REQUEST.getBytes();
		} else if (!request.getHttpRequestType().equals("GET")) {
			// create HttpResponse 400 BAd request
			status = SC_BAD_REQUEST;
			fileBuffer = SC_BAD_REQUEST.getBytes();
		} else if (!request.getHttpVersion().equals("HTTP/1.0") && !request.getHttpVersion().equals("HTTP/1.1")) {
			// create HttpResponse 400 BAd request
			status = SC_BAD_REQUEST;
			fileBuffer = SC_BAD_REQUEST.getBytes();
		} else {
			// All condition satisfied. Now checking for resource. 
			String uri = null;
			if (request.getRequestUri().equals("/")) {
				uri = "/index.html";
			} else {
				uri = request.getRequestUri();
			}
			
			uri = uri.substring(1);
			fileExtension = uri.substring(uri.indexOf('.') + 1);
			String uriPath = request.getRootDir() + "/" + uri;
			File resource = new File(uriPath);
			
			if (!resource.exists()) {
				status = SC_NOT_FOUND;
				fileBuffer = SC_NOT_FOUND.getBytes();
			} else {
				if (resource.isDirectory()) {
					status = SC_BAD_REQUEST;
					fileBuffer = SC_BAD_REQUEST.getBytes();
				} else {
					if (resource.canRead()) {
						try {
							FileInputStream fis = new FileInputStream(resource);
							fileBuffer = new byte[fis.available()];
							int bytesRead = fis.read(fileBuffer);
							if (bytesRead != -1) {
								status = SC_OK;
							} else {
								status = SC_NOT_FOUND;
								fileBuffer = SC_NOT_FOUND.getBytes();
							}
							fis.close();
						} catch (FileNotFoundException e) {
							status = SC_NOT_FOUND;
							fileBuffer = SC_NOT_FOUND.getBytes();
						} catch (IOException e) {
							status = SC_INTERNAL_SERVER_ERROR;
							fileBuffer = SC_INTERNAL_SERVER_ERROR.getBytes();
						}
					} else {
						status = SC_FORBIDDEN;
						fileBuffer = SC_FORBIDDEN.getBytes();
					}
				}
			}
		}
		
		String httpVersion = HTTP_VERSION_1_1;
		if(request.getHttpVersion().equals(HTTP_VERSION_1_0) || request.getHttpVersion().equals(HTTP_VERSION_1_1)){
			httpVersion = request.getHttpVersion();
		}

		final Map<String, String> responseHeaders = new HashMap<>();
		responseHeaders.put("Date", getServerDateTime());
		if(fileBuffer != null){
			responseHeaders.put("Content-Length", ""
					+ fileBuffer.length);
		}
		if(contentType.containsKey(fileExtension) && status.equals(SC_OK)){
			responseHeaders.put("Content-Type", contentType.get(fileExtension));
		} else {
			responseHeaders.put("Content-Type", contentType.get("txt"));
		}
		String connectionStatus;
		if(request.isKeepAlive()){
			connectionStatus = "keep-alive";
		}
		else{
			connectionStatus = "close";
		}
		responseHeaders.put("Connection", connectionStatus);
		HttpResponse response = new HttpResponse(httpVersion, status,
				responseHeaders, fileBuffer);
		return response;
	}
	
	private String getServerDateTime() {
	    Calendar calendar = Calendar.getInstance();
	    SimpleDateFormat df = new SimpleDateFormat(
	        "EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
	    
	    return df.format(calendar.getTime());
	}

}
