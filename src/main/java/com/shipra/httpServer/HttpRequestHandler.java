package com.shipra.httpServer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author shipra
 *
 */
public class HttpRequestHandler implements Runnable {
	/** Socket on which client will be communicating with server. */
	private Socket sockFd = null;
	/** Takes root directory. */
	private final String rootDir;
	/** Counter for maintaining active thread count in system. */
	private AtomicInteger threadCount;

	/**
	 * Parameterized Constructor
	 * 
	 * @param sockFd
	 *            socket file description on which server is communicating with
	 *            client
	 * @param rootDir
	 *            directory path
	 * @param threadCount
	 *            number of active threads in system
	 */
	public HttpRequestHandler(Socket sockFd, String rootDir,
			AtomicInteger threadCount) {
		this.sockFd = sockFd;
		this.rootDir = rootDir;
		this.threadCount = threadCount;
	}

	/**
	 * Class implements Runnable interface. This method is implementation of
	 * run().
	 */
	public void run() {
		try {
			HttpRequest request;
			InputStream is = null;
			OutputStream os = null;
			do {
				is = sockFd.getInputStream();
				request = new HttpRequest(rootDir, is);
				System.out.println(request.toString());
				HttpRequestProcessor processor = new HttpRequestProcessor(
						request);
				HttpResponse response = processor.requestProcessor();

				os = sockFd.getOutputStream();
				response.writeResponse(os);
			} while (request.isKeepAlive());

			if (is != null) {
				is.close();
			}
			if (os != null) {
				os.close();
			}
		} catch (IOException e) {
			System.out.println(e.getMessage() + " Total thread count "
					+ threadCount.get());
		} finally {
			try {
				sockFd.close();
			} catch (IOException e) {
			}
			threadCount.decrementAndGet();
		}
	}

}
