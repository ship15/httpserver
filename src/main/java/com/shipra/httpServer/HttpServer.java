package com.shipra.httpServer;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author shipra
 *
 *
 */
public class HttpServer {
	/** port number at which server binds and listens. */
	private final int port;
	/** backlog is size of queue where client requests line up. */
	private final int backlog = 100;
	/** Instance of ServerSocket. */
	private ServerSocket serverSocket = null;
	/** Path of root directory where resources are present. */
	private String rootDir;
	/** Counter maintaining the number of active threads in the system. */
	/**
	 * This variable of AtomicInteger class maintains the count of active
	 * threads in system and handles the race condition when multiple threads
	 * try to update this count variable.
	 */
	private AtomicInteger threadCount;
	/**
	 * This defines the default connection timeout.
	 */
	private int connectionTimeout = 1000;

	/**
	 * Parameterized constructor.
	 * 
	 * @param directoryPath
	 *            root directory path where resources are present.
	 * @param portNumber
	 *            port number on which server binds and listens to client's
	 *            request.
	 * @throws IOException
	 *             if an I/O error occurs when opening the socket.
	 */
	public HttpServer(String directoryPath, int portNumber) throws IOException {
		port = portNumber;
		rootDir = directoryPath;
		threadCount = new AtomicInteger();
		serverSocket = new ServerSocket(port, backlog);
	}

	/**
	 * This method accepts the client request on server Socket and generate the
	 * child socket for this request. It creates a separate thread to serve each
	 * client request and also maintain the total count of active threads
	 * serving clients. And it determines timeout for each HTTP connection.
	 */
	public void serverHandle() {
		while (true) {
			try {
				System.out.println("Waiting for connections and total threads "
						+ threadCount.get());
				Socket sockFd = serverSocket.accept();
				threadCount.incrementAndGet();
				if (threadCount.get() <= 10) {
					connectionTimeout = 90000;
				} else if (threadCount.get() <= 20) {
					connectionTimeout = 15000;
				} else if (threadCount.get() <= 25) {
					connectionTimeout = 5000;
				}
				sockFd.setSoTimeout(connectionTimeout);
				if (sockFd != null) {
					Thread thread = new Thread(new HttpRequestHandler(sockFd,
							rootDir, threadCount));
					thread.start();
				}
			} catch (IOException e) {
				System.out.println(e.getMessage() + " Total thread count "
						+ threadCount.get());
			}
		}

	}

	/**
	 * @param path
	 *            directory path
	 * @return if input path is directory or not
	 */
	public static boolean isDirectory(String path) {
		File fs = new File(path);
		if (fs.exists()) {
			return fs.isDirectory();
		}
		return false;
	}

	/**
	 * Entry point for HTTP Server
	 * 
	 * @param args
	 *            command line arguments
	 * @throws Exception
	 *             when invalid arguments are passed.
	 */
	public static void main(String[] args) throws Exception {
		if (args.length != 4) {
			throw new Exception(
					"Wrong Syntax.\n Correct syntax: -document_root \"directory path\" -port portNumber \n");
		}
		String directory;
		int portNumber;

		if ("-document_root".equals(args[0])) {
			directory = args[1];
		} else {
			throw new Exception(
					"Wrong Syntax.\n Correct syntax: -document_root \"directory path\" -port portNumber \n");
		}

		if ("-port".equals(args[2])) {
			portNumber = Integer.parseInt(args[3]);
		} else {
			throw new Exception(
					"Wrong Syntax.\n Correct syntax: -document_root \"directory path\" -port portNumber \n");
		}

		if (!isDirectory(directory)) {
			throw new Exception("Invalid directory. Please give correct path");
		}
		if (portNumber < 8000 || portNumber > 9999) {
			throw new Exception(
					"Invalid port number. Port number range is 8000 to 9999");
		}

		HttpServer server = new HttpServer(directory, portNumber);
		System.out.println("Started HttpServer on port " + portNumber);
		System.out.println("To stop HttpServer - Press Ctrl-c");
		server.serverHandle();
	}
}
