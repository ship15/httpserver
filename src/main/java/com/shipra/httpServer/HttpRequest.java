package com.shipra.httpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author shipra
 *
 */
public class HttpRequest {
	private String httpRequestType = null;
	private String requestUri = null;
	private String httpVersion = null;
	private final String rootDir;
	private boolean keepAlive;
	private Map<String, String> httpHeaders = new HashMap<>();

	public HttpRequest(String rootDir, InputStream is) throws IOException {
		this.rootDir = rootDir;
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		String buffer = reader.readLine();
		while(buffer == null || buffer.length() == 0) {
			buffer = reader.readLine();
		}
		String[] requestLine = buffer.split("\\s");
		if (requestLine.length == 3) {
			this.httpRequestType = requestLine[0].trim();
			this.requestUri = requestLine[1].trim();
			this.httpVersion = requestLine[2].trim();
		}

		do {
			buffer = reader.readLine();
			if (buffer != null && buffer.length() > 0) {
				String[] headerLine = buffer.split(": ");
				if (headerLine.length == 2) {
					httpHeaders.put(headerLine[0], headerLine[1]);
				}
			}
		} while (buffer != null && buffer.length() > 0);

		if (httpHeaders.containsKey("Connection")) {
			if (httpHeaders.get("Connection").equals("keep-alive")) {
				keepAlive = true;
			} else if (httpHeaders.get("Connection").equals("close")) {
				keepAlive = false;
			}
		} else {
			if (httpVersion == null || httpVersion.equals("HTTP/1.0")) {
				keepAlive = false;
			} else {
				keepAlive = true;
			}
		}
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(httpRequestType).append(" ").append(requestUri).append(" ").append(httpVersion).append("\n");
		Set<String> keys = httpHeaders.keySet();
		for (String key : keys) {
			sb.append(key).append(" : ").append(httpHeaders.get(key)).append("\n");
		}
		return sb.toString();
	}

	public boolean isKeepAlive() {
		return keepAlive;
	}

	public String getRootDir() {
		return rootDir;
	}

	public String getHttpRequestType() {
		return httpRequestType;
	}

	public String getRequestUri() {
		return requestUri;
	}

	public String getHttpVersion() {
		return httpVersion;
	}

	public Map<String, String> getHttpHeaders() {
		return httpHeaders;
	}

}
