package com.shipra.httpServer;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author shipra
 */
public class HttpResponse {
	private String httpVersion = null;
	private String status = null;
	private Map<String, String> responseHeaders = null;
	private byte[] responseBody = null;

	public HttpResponse(String httpVersion, String status,
			Map<String, String> responseHeaders, byte[] responseBody) {
		this.httpVersion = httpVersion;
		this.status = status;
		this.responseHeaders = responseHeaders;
		this.responseBody = responseBody;
	}

	public void writeResponse(OutputStream os) throws IOException {
		DataOutputStream dos = new DataOutputStream(os);
		StringBuilder statusLine = new StringBuilder();
		statusLine.append(httpVersion).append(" ").append(status).append("\n");
		dos.write(statusLine.toString().getBytes());

		Set<String> keySet = responseHeaders.keySet();
		Iterator<String> itr = keySet.iterator();
		while (itr.hasNext()) {
			String headerName = itr.next();
			String headerValue = responseHeaders.get(headerName);
			String headerLine = headerName + ": " + headerValue + "\n";
			dos.write(headerLine.getBytes());
		}
		// Blank line separating header & body.
		dos.write("\n".getBytes());
		if (responseBody != null && responseBody.length > 0) {
			dos.write(responseBody);
		}
		dos.write("\n".getBytes());
		dos.flush();
	}
}
